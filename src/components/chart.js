import React from "react";
import PropTypes from "prop-types";
import Chart from "react-google-charts";
import { kelvinToCelcius, kelvinToFehrenheit } from "../utils/converter";
import { WeatherDataPointSchema } from "../redux/model";

export default class BarChart extends React.Component {
  render() {
    const { data, unit } = this.props;
    const bars = [["Time", "Temp"]];
    data.forEach(element => {
      const time = new Date(element.dt * 1000).toLocaleTimeString();
      const temp =
        unit === "c"
          ? kelvinToCelcius(element.main.temp)
          : kelvinToFehrenheit(element.main.temp);
      bars.push([time, temp]);
    });
    return (
      <div>
        <Chart chartType="Bar" loader={<div>Loading Chart</div>} data={bars} />
      </div>
    );
  }
}

BarChart.propTypes = {
  data: PropTypes.arrayOf(WeatherDataPointSchema).isRequired,
  unit: PropTypes.oneOf(["c", "f"]).isRequired
};
