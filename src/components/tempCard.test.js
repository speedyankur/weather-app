// counter.test.js
import React from "react";
import { createStore } from "redux";
// We're using our own custom render function and not RTL's render
// our custom utils also re-export everything from RTL
// so we can import fireEvent and screen here as well
import "@testing-library/jest-dom/extend-expect";
import TempCard from "./tempCard";
import { render, fireEvent, screen, cleanup } from "../utils/test-utils";
import { kelvinToCelcius } from "../utils/converter";

afterEach(cleanup);

const data = [
  {
    dt: 1591660800,
    main: {
      temp: 284.58
    },
    weather: [{ icon: "10n" }],
    dt_txt: "2020-06-09 00:00:00"
  },
  {
    dt: 1591671600,
    main: {
      temp: 284.58
    },
    weather: [{ icon: "10n" }],
    dt_txt: "2020-06-09 03:00:00"
  }
];

it("renders date label", () => {
  const { getByText } = render(<TempCard data={data} unit={"c"} />);
  const date = new Date(data[0].dt * 1000).toLocaleDateString();
  const dateElement = getByText(date);
  expect(dateElement).toBeInTheDocument();
});

it("should calculate avg temprature and render", () => {
  const { getByText } = render(<TempCard data={data} unit={"c"} />);
  const avgTemp = (
    data.reduce((a, b) => a + b.main.temp, 0) / data.length
  ).toFixed(2);
  const temp = kelvinToCelcius(avgTemp).toFixed(2) + "°C";
  const temperatureElement = getByText(temp);
  expect(temperatureElement).toBeInTheDocument();
});

it("should select the card on click", () => {
  const { getByText, container } = render(<TempCard data={data} unit={"c"} />);
  const cardBefore = container.firstChild;
  expect(cardBefore).not.toHaveClass("MuiPaper-outlined");
  const avgTemp = (
    data.reduce((a, b) => a + b.main.temp, 0) / data.length
  ).toFixed(2);
  const temp = kelvinToCelcius(avgTemp).toFixed(2) + "°C";
  const temperatureElement = getByText(temp);
  fireEvent.click(temperatureElement);
  const cardAfter = container.firstChild;
  expect(cardAfter).toHaveClass("MuiPaper-outlined");
});
