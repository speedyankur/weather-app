import { Grid, Button } from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import TempCard from "./tempCard";
import { WeatherDataPointSchema } from "../redux/model";

export default class CardsGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
      pageSize: 3,
      totalPages: Math.ceil(Object.values(props.data).length / 3)
    };
  }
  goPrev() {
    const nextCurrentPage = this.state.currentPage - 1;
    this.setState({ ...this.state, currentPage: nextCurrentPage });
  }
  goNext() {
    const nextCurrentPage = this.state.currentPage + 1;
    this.setState({ ...this.state, currentPage: nextCurrentPage });
  }
  render() {
    const { currentPage, pageSize, totalPages } = this.state;
    const { data, unit } = this.props;
    const startIndex = currentPage * pageSize;
    const endIndex = startIndex + pageSize;
    const currentPageCards = data.slice(startIndex, endIndex);
    return (
      <div>
        <Grid container spacing={2} justify={"space-between"}>
          <Grid item xs={2}>
            <Button
              color="primary"
              size="large"
              onClick={e => this.goPrev()}
              disabled={currentPage === 0}
              data-testid="prev-btn"
              startIcon={<NavigateBeforeIcon />}
            >
              Prev
            </Button>
          </Grid>
          <Grid item xs={2} container justify={"flex-end"}>
            <Button
              color="primary"
              size="large"
              onClick={e => this.goNext()}
              disabled={totalPages - 1 === currentPage}
              data-testid="next-btn"
              endIcon={<NavigateNextIcon />}
            >
              Next
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={0} justify={"space-evenly"}>
          {currentPageCards.map((cardData, index) => (
            <Grid item key={index}>
              <TempCard data={cardData} unit={unit} />
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

CardsGrid.propTypes = {
  data: PropTypes.arrayOf(PropTypes.arrayOf(WeatherDataPointSchema)).isRequired,
  unit: PropTypes.oneOf(["c", "f"]).isRequired
};
