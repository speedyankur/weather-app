// counter.test.js
import React from "react";
import { createStore } from "redux";
// We're using our own custom render function and not RTL's render
// our custom utils also re-export everything from RTL
// so we can import fireEvent and screen here as well
import "@testing-library/jest-dom/extend-expect";
import BarChart from "./chart";
import { render, cleanup } from "@testing-library/react";

afterEach(cleanup);
const data = [
  {
    dt: 1591660800,
    main: {
      temp: 284.58
    },
    weather: [{ icon: "10n" }],
    dt_txt: "2020-06-09 00:00:00"
  }
];
it("renders chart component", () => {
  const { getByText } = render(<BarChart data={data} unit={"c"} />);
  const el = getByText(/Loading Chart/i);
  expect(el).toBeInTheDocument();
});
