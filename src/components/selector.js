import { Radio, RadioGroup, FormControlLabel } from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { changeUnit } from "../redux/action";

const Selector = props => (
  <RadioGroup
    aria-label="select unit"
    name="unit"
    value={props.unit}
    onChange={props.changeUnit}
  >
    <FormControlLabel value="c" control={<Radio />} label="Celcius" />
    <FormControlLabel value="f" control={<Radio />} label="Fahrenheit" />
  </RadioGroup>
);

Selector.propTypes = {
  unit: PropTypes.oneOf(["c", "f"]).isRequired
};

const mapStateToProps = ({ weather }) => ({
  unit: weather.unit
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changeUnit
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Selector);
