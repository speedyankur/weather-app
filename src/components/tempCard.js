import { Card, CardContent, Typography, ButtonBase } from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { onCardSelection } from "../redux/action";
import { kelvinToCelcius, kelvinToFehrenheit } from "../utils/converter";
import { WeatherDataPointSchema } from "../redux/model";

class TempCard extends React.Component {
  render() {
    const { data, unit, onCardSelection, daySelected } = this.props;
    const avgTemp = (
      data.reduce((a, b) => a + b.main.temp, 0) / data.length
    ).toFixed(2);
    const temp =
      unit === "c"
        ? kelvinToCelcius(avgTemp).toFixed(2) + "°C"
        : kelvinToFehrenheit(avgTemp).toFixed(2) + "°F";
    return (
      <Card
        variant={daySelected === data ? "outlined" : "elevation"}
        data-testid="temp-card"
      >
        <ButtonBase
          onClick={event => {
            onCardSelection(data);
          }}
        >
          <CardContent>
            {data[0] && data[0].weather && (
              <div>
                <img
                  className="img-fluid"
                  alt="weather-icon"
                  src={`https://openweathermap.org/img/w/${data[0].weather[0].icon}.png`}
                />
              </div>
            )}
            <Typography>
              {new Date(data[0].dt * 1000).toLocaleDateString()}
            </Typography>
            <Typography color="textSecondary" gutterBottom>
              {temp}
            </Typography>
          </CardContent>
        </ButtonBase>
      </Card>
    );
  }
}

TempCard.propTypes = {
  data: PropTypes.arrayOf(WeatherDataPointSchema).isRequired,
  unit: PropTypes.oneOf(["c", "f"]).isRequired,
  onCardSelection: PropTypes.func
};

const mapStateToProps = ({ weather }) => ({
  daySelected: weather.daySelected
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onCardSelection
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TempCard);
