// counter.test.js
import React from "react";
import { createStore } from "redux";
// We're using our own custom render function and not RTL's render
// our custom utils also re-export everything from RTL
// so we can import fireEvent and screen here as well
import "@testing-library/jest-dom/extend-expect";
import CardsGrid from "./cardsGrid";
import { render, fireEvent, screen, cleanup } from "../utils/test-utils";
import { kelvinToCelcius } from "../utils/converter";

afterEach(cleanup);

const data = [
  [
    {
      dt: 1591660800,
      main: {
        temp: 284.58
      },
      weather: [{ icon: "10n" }],
      dt_txt: "2020-06-09 00:00:00"
    },
    {
      dt: 1591671600,
      main: {
        temp: 284.58
      },
      weather: [{ icon: "10n" }],
      dt_txt: "2020-06-09 03:00:00"
    }
  ],
  [
    {
      dt: 1591747200,
      main: {
        temp: 284.58
      },
      weather: [{ icon: "10n" }],
      dt_txt: "2020-06-10 00:00:00"
    },
    {
      dt: 1591758000,
      main: {
        temp: 284.58
      },
      weather: [{ icon: "10n" }],
      dt_txt: "2020-06-10 03:00:00"
    }
  ]
];

it("should renders 2 card", () => {
  const { getAllByTestId } = render(<CardsGrid data={data} unit={"c"} />);
  const tempCards = getAllByTestId("temp-card");
  expect(tempCards.length).toBe(2);
});

it("should disable next-btn for 2 cards", () => {
  const { getByTestId } = render(<CardsGrid data={data} unit={"c"} />);
  const nextBtn = getByTestId("next-btn");
  expect(nextBtn).toBeDisabled();
});
