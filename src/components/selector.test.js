// counter.test.js
import React from "react";
import { createStore } from "redux";
// We're using our own custom render function and not RTL's render
// our custom utils also re-export everything from RTL
// so we can import fireEvent and screen here as well
import "@testing-library/jest-dom/extend-expect";
import Selector from "./selector";
import { render, fireEvent, cleanup } from "../utils/test-utils";

afterEach(cleanup);

it("renders 2 selector component", () => {
  const { getByText } = render(<Selector />);
  const celciusElement = getByText(/Celcius/i);
  expect(celciusElement).toBeInTheDocument();
  const fahrenheitElement = getByText(/Fahrenheit/i);
  expect(fahrenheitElement).toBeInTheDocument();
});

it("should autoselect correct element from initialState", () => {
  const { container } = render(<Selector />);
  const el = container.querySelector("input[value=f]");
  expect(el).toBeChecked();
});

it("should select correct temp on selection", () => {
  const { container } = render(<Selector />);
  const celciusEl = container.querySelector("input[value=c]");
  fireEvent.click(celciusEl);
  expect(celciusEl).toBeChecked();
});
