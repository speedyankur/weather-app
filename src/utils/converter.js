export function kelvinToFehrenheit(kelvin) {
  return ((kelvin - 273.15) * 9) / 5 + 32;
}
export function kelvinToCelcius(kelvin) {
  return kelvin - 273.15;
}
