import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Weather from "../weather";
import Loader from "../loader";
import { Container } from "@material-ui/core";

const App = () => (
  <Container maxWidth="md">
    <div>
      <main>
        <Switch>
          <Route path="/weather-app" component={Weather} />
          <Route exact path="/" component={Loader} />
          <Redirect from="/*" to="/" />
        </Switch>
      </main>
    </div>
  </Container>
);
export default App;
