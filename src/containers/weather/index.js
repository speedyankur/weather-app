import React from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";
import Selector from "../../components/selector";
import CardsGrid from "../../components/cardsGrid";
import Chart from "../../components/chart";
import { WeatherDataPointSchema } from "../../redux/model";

class Weather extends React.Component {
  render() {
    const { daySelected, data, weatherLoaded, unit } = this.props;
    return (
      <div>
        <h1>Munich, DE - Weekly Weather App</h1>
        <Selector />
        <CardsGrid data={data} unit={unit} />
        <br />
        <br />
        {daySelected.length > 0 && (
          <Grid container spacing={2} justify={"center"}>
            <Grid item xs={8}>
              <Chart data={daySelected} unit={unit} />
            </Grid>
          </Grid>
        )}
        {!weatherLoaded && (
          <Redirect
            to={{
              pathname: "/"
            }}
          />
        )}
      </div>
    );
  }
}

Weather.propTypes = {
  weatherLoaded: PropTypes.bool.isRequired,
  data: PropTypes.arrayOf(PropTypes.arrayOf(WeatherDataPointSchema)).isRequired,
  daySelected: PropTypes.arrayOf(WeatherDataPointSchema).isRequired
};

const mapStateToProps = ({ weather }) => ({
  unit: weather.unit,
  weatherLoaded: weather.isLoaded,
  data: weather.data,
  daySelected: weather.daySelected
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Weather);
