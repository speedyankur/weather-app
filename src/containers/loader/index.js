import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchWeather } from "../../redux/action";
import { CircularProgress } from "@material-ui/core";

class Loader extends React.Component {
  render() {
    return (
      <div>
        <h1>
          Loading... <CircularProgress />
        </h1>
        <p>Please wait</p>
      </div>
    );
  }
  componentDidMount() {
    this.props.fetchWeather();
  }
}

Loader.propTypes = {
  fetchWeather: PropTypes.func
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchWeather
    },
    dispatch
  );

export default connect(state => state, mapDispatchToProps)(Loader);
