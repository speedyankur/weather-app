// counter.test.js
import React from "react";
import { createStore } from "redux";
// We're using our own custom render function and not RTL's render
// our custom utils also re-export everything from RTL
// so we can import fireEvent and screen here as well
import { render, fireEvent, screen } from "../../utils/test-utils";
import "@testing-library/jest-dom/extend-expect";
import Loader from "./";

test("renders Loader component", () => {
  const { getByText } = render(<Loader />);
  const linkElement = getByText(/Loading.../i);
  expect(linkElement).toBeInTheDocument();
});
