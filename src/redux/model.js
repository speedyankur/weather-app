import PropTypes from "prop-types";

export const WeatherDataPointSchema = PropTypes.shape({
  dt: PropTypes.number,
  main: PropTypes.shape({
    temp: PropTypes.number,
    feels_like: PropTypes.number,
    temp_min: PropTypes.number,
    temp_max: PropTypes.number,
    pressure: PropTypes.number,
    sea_level: PropTypes.number,
    grnd_level: PropTypes.number,
    humidity: PropTypes.number,
    temp_kf: PropTypes.number
  }),
  weather: PropTypes.array,
  clouds: PropTypes.object,
  wind: PropTypes.object,
  sys: PropTypes.object,
  dt_txt: PropTypes.string
});
