import { push } from "connected-react-router";
import {
  WEATHER_REQUESTED,
  WEATHER_FETCHED,
  WEATHER_ERROR,
  WEATHER_UNIT_CHANGE,
  WEATHER_CARD_SELECTION
} from "./contants";

const API_KEY = "75f972b80e26f14fe6c920aa6a85ad57";
const api = `https://api.openweathermap.org/data/2.5/forecast?q=Munich,de&APPID=${API_KEY}&cnt=40`;

export const fetchWeather = () => {
  return dispatch => {
    dispatch({
      type: WEATHER_REQUESTED
    });
    return fetch(api)
      .then(response => response.json())
      .then(json => {
        //make a empty map for daily data
        var dailyData = {};
        json.list.map(item => {
          const dateTime = new Date(item.dt * 1000);
          const day = dateTime.getDate();
          // check if dailyData map has it
          if (!dailyData[day]) dailyData[day] = [];
          dailyData[day].push(item);
          return 1;
        });
        dailyData = Object.values(dailyData);
        dispatch({
          type: WEATHER_FETCHED,
          payload: dailyData
        });
        dispatch(push("/weather-app"));
      })
      .catch(error => {
        dispatch({
          type: WEATHER_ERROR
        });
      });
  };
};

export const changeUnit = e => {
  return dispatch => {
    dispatch({
      type: WEATHER_UNIT_CHANGE,
      payload: e.target.value
    });
  };
};

export const onCardSelection = cardData => {
  return dispatch => {
    dispatch({
      type: WEATHER_CARD_SELECTION,
      payload: cardData
    });
  };
};
