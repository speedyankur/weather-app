import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import weather from "./reducer";

const createRootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    weather
  });
export default createRootReducer;
