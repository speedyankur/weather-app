export const WEATHER_REQUESTED = "weather/REQUESTED";
export const WEATHER_FETCHED = "weather/FETCHED";
export const WEATHER_ERROR = "weather/ERROR";

export const WEATHER_UNIT_CHANGE = "weather/UNIT_CHANGE";
export const WEATHER_CARD_SELECTION = "weather/CARD_SELECTION";
