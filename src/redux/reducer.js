import {
  WEATHER_REQUESTED,
  WEATHER_FETCHED,
  WEATHER_ERROR,
  WEATHER_UNIT_CHANGE,
  WEATHER_CARD_SELECTION
} from "./contants";

export const initialState = {
  isLoading: false,
  isLoaded: false,
  isError: false,
  unit: "f",
  daySelected: [],
  data: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case WEATHER_REQUESTED:
      return {
        ...state,
        isLoading: true,
        isError: false
      };
    case WEATHER_FETCHED:
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        isError: false,
        data: action.payload
      };
    case WEATHER_ERROR:
      return {
        ...state,
        isLoading: false,
        isLoaded: false,
        isError: true
      };
    case WEATHER_UNIT_CHANGE:
      return {
        ...state,
        unit: action.payload
      };
    case WEATHER_CARD_SELECTION:
      return {
        ...state,
        daySelected: action.payload
      };

    default:
      return state;
  }
};
